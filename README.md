﻿**Welcome to the public repository for the guild bank of Vitality - Benediction (NA)**

This repository is being maintained by <Loro> and <Rousseau> using Git for Windows. You do not need to know anything about Git to use this page. All information shall be stored in text files so that interested parties can quickly check how the bank contents have changed using a feature called 'git diff'.  Additionally, all files are snapshotted with dates automatically, to help reduce clutter.

*A quick way to check this would be to click on the "Commits" tab on the left side-bar, then choose the top-most entry to see what files were changed, and what line additions/deletions occurred within them. Green sections highlight new lines, and added text. Red sections highlight deleted lines, or changes to old ones.*

This is particularly useful, since it guarentees that nobody is going back and changing transactions to sneak items out of the bank.

---

## Sending items to the bank

Mail in-game items to Pinbankraid - Benediction (NA).
I haven't decided which items should and should not live in the bank yet... there is a chance I will convert items you send to money and store that instead. Please bare with me in this process of learning :)  
Additional information on rewards TBD.

---

## Withdrawing items from the bank

Rules TBD.

---

## Making changes to this repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

I personally use **Git for Windows** and **Git Bash**. As a general rule for git respositories, you should never make commits directly to the Master Branch. You should always make changes to your own personal branch (blink_master, etc.) and then make a pull request for me to review your changes. If you have any interest in doing this at all, send me a DM and I'll help you get started with the basics.  